<?php

$path = "../";
include($path."../_t2/includes/session_start.php");  				// path good
include($path."configuration/environment_settings.php");			// path good
include($path."includes/should_errors_display.php");
include($path."../_t2/database/sql_functions.php");
ini_set('display_errors',1);  
error_reporting(E_ALL);
$campaign_id = $_REQUEST["cid"];
$phone = $_REQUEST["phone"];
$sql = "
select * from contact_data where phone = ?
";
$params = array($phone);
$results = sql_shell($sql, $params, $path);
$return = array();
$return["campaign"] = $campaign_id;
$return["search_type"] = "phone";
$return["search_value"] = $phone;
$return["customer_rowcount"] = $results["rowcount"];

if($results["rowcount"] == 1)
	{	
	$return["customer_id"] = $results["recordset"][0]["customer_id"];
	}
elseif($results["rowcount"] > 1)
	{
	$return["customer_recordset"] = $results["recordset"];	
	}
echo(json_encode($return));

?>