<?php

$path = "../";
include($path."../_t2/includes/session_start.php");  				// path good
include($path."configuration/environment_settings.php");			// path good
include($path."includes/should_errors_display.php");
include($path."../_t2/database/sql_functions.php");
$customer_id = $_REQUEST["customer_id"];
$sql = "
select * from contact_data where customer_id = ?
";
$params = array($customer_id);
$results = sql_shell($sql, $params, $path);
/*echo($sql."<br />");
echo("<textarea style='width: 100%; height: 300px;'>");
print_r($results);
echo("</textarea>");*/
$lang = array();
$lang["fn_placeholder"] = "First Name";
$lang["mn_placeholder"] = "M.I.";
$lang["ln_placeholder"] = "Last Name";
$lang['cart_shipping_company_placeholder'] = "Company";
$lang["addr1_placeholder"] = "Address";
$lang["addr2_placeholder"] = "Additional Address Information";
$lang["city_placeholder"] = "City";
$lang["state_placeholder"] = "- Select State -";
$lang["province_placeholder"] = "- Select Province -";
$lang["division_placeholder"] = "State / Province / Division";
$lang["zip_placeholder"] = "Postal Code";
$lang["country_placeholder"] = "- Please Select Country -";
$lang["phone_placeholder"] = "Phone Number";
$lang["email_placeholder"] = "Email Address";
$address_type = "bill";
$fn = $results["recordset"][0]["first_name"];
$mn = $results["recordset"][0]["middle_name"];
$ln = $results["recordset"][0]["last_name"];
$company = $results["recordset"][0]["company"];
$address1 = $results["recordset"][0]["address1"];
$address2 = $results["recordset"][0]["address2"];
$city = $results["recordset"][0]["city"];
$state = $results["recordset"][0]["state_prov"];
$zip = $results["recordset"][0]["postal_code"]; 
$country = $results["recordset"][0]["country"];
$phone = $results["recordset"][0]["phone"];
$email = $results["recordset"][0]["email"];
$validated_javascript = "yes";
$validated_fedex = "yes";
$validated_javascript = "yes";
$validated_overall = "yes";
$onchange = "";
?>
<div class=pop-template>
	<div>&nbsp;</div><div>&nbsp;</div>
	<div class=pop-title>Customer Contact Information</div>
	<div class=pop-side>
		<a href="javascript: save_customer();">Save Customer Information</a>
		
	</div>
	<div class=pop-content>
<center>
<div style="height: 340px;">
	<div style="width: 500px; height: 310px; overflow: hidden;">
		<div style="position: relative; top: 0; left: 0; width: 500px; height: 310px; overflow: hidden;">
<?php
include($path."../_t2/includes/address_only.php");
include($path."../_t2/elements/phone.php");
include($path."../_t2/elements/email.php");
?>
		<input type=hidden id=customer_id value="<?php echo($customer_id); ?>" />
		</div>
	</div>
</div>
</center>
	</div>
</div>