<?php

$path = "../";
include($path."../_t2/includes/session_start.php");  				// path good
include($path."configuration/environment_settings.php");			// path good
include($path."includes/should_errors_display.php");
include($path."../_t2/database/sql_functions.php");
include($path."includes/calculations.php");
ini_set('display_errors',1);  
error_reporting(E_ALL);
$order_id = $_REQUEST["order_id"];
$sql = "
select
	order_id,
	purchase_id,
	status,
	status_date,
	transaction_date,
	transaction_amount,
	cart_total,
	tax_total,
	shipping_total,
	convenience_fee,
	greek_fee,
	other_fee1,
	other_fee1_description,
	other_fee2,
	other_fee2_description,
	other_fee3,
	other_fee3_description,
	promotional_discount,
	promotional_code,
	non_profit,
	credit_card_month,
	credit_card_year,
	credit_card_owner,
	credit_card_type,
	authorization_number,
	response_code,
	result_code,
	result_message,
	entered_by,
	notes, 
	notes_author,
	notes_last_updated
from
	orders
where 
	order_id = ?
";
$params = array($order_id);
$results = sql_shell($sql, $params, $path);
/*echo($sql."<br />");
echo("<textarea style='width: 100%; height: 250px;'>");
print_r($results);
echo("</textarea>");
exit();*/
$payment = $results["recordset"][0];
$htm = "";
$del = "";
$pmt = array();
$amounts = array();
$amounts["Cart Total"] = $payment["cart_total"];
$amounts["Tax Total"] = $payment["tax_total"];
$amounts["Shipping Total"] = $payment["shipping_total"];
$amounts["Convenience Fee"] = $payment["convenience_fee"];
$amounts["Greek Lettering Fee"] = $payment["greek_fee"];
$of1 = "Other Fee 1";
if($payment["other_fee1_description"] != ""){ $of1 = $payment["other_fee1_description"]; }
$amounts[$of1] = $payment["other_fee1"];
$of2 = "Other Fee 2";
if($payment["other_fee2_description"] != ""){ $of2 = $payment["other_fee2_description"]; }
$amounts[$of2] = $payment["other_fee2"];
$of3 = "Other Fee 3";
if($payment["other_fee3_description"] != ""){ $of3 = $payment["other_fee3_description"]; }
$amounts[$of3] = $payment["other_fee3"];
$amounts["Promotional Discount"] = $payment["promotional_discount"];
$amounts["Total Transaction Amount"] = $payment["transaction_amount"];
$htm = "
<div class='pmt-group blue'>
<center>
	<span class=pmt-title>Payment Amounts</span>
</center>
<br />";
foreach($amounts as $prop => $val)
	{
	if($prop == "Total Transaction Amount"){ $htm .= "<div class=hzline></div>"; }
	$htm .= "
<span class=pmt-prop>".$prop."</span><span class=pmt-amt-val>".number_format((float) $val, 2, ".", ",")."</span><br />
";
	}
$htm .= "
<br />
</div>
";

$status = array();
$status["Status"] = $payment["status"];
$status["Status Date"] = $payment["status_date"];
$status["Authorization Number"] = $payment["authorization_number"];
$status["Response Code"] = $payment["response_code"];
$status["Result Code"] = $payment["result_code"];
$status["Result Message"] = $payment["result_message"];
$htm .= "
<div class='pmt-group purple'>
<center>
	<span class=pmt-title>Processing Results</span>
</center>
<br />";
foreach($status as $prop => $val)
	{
	$htm .= "
<span class=pmt-prop>".$prop."</span><span class=pmt-val>".$val."</span><br />
";
	}
$htm .= "
<br />
</div>
";
$cc = array();
$cc["Credit Card Owner"] = $payment["credit_card_owner"];
$cc["Credit Card Number"] = "";
$cc["Credit Card Type"] = $payment["credit_card_type"];
$cc["Credit Card Month"] = $payment["credit_card_month"];
$cc["Credit Card Year"] = $payment["credit_card_year"];
$htm .= "
<div class='pmt-group red'>
<center>
	<span class=pmt-title>Credit Card Information</span>
</center>
<br />";
foreach($cc as $prop => $val)
	{
	$htm .= "
<span class=pmt-prop>".$prop."</span><span class=pmt-val>".$val."</span><br />
";
	}
$htm .= "
<br />
</div>
";
$details = array();
$details["Order ID"] = $payment["order_id"];
$details["Purchase ID"] = $payment["purchase_id"];
$details["Transaction Date"] = $payment["transaction_date"];
$details["Promotional Code"] = $payment["promotional_code"];
$details["Non Profit"] = $payment["non_profit"];
$details["Entered By"] = $payment["entered_by"];
$details["Notes"] = $payment["notes"];
$details["Notes Author"] = $payment["notes_author"];
$details["Notes Date"] = $payment["notes_last_updated"];
$htm .= "
<div class='pmt-group green'>
<center>
	<span class=pmt-title>Payment Details</span>
</center>
<br />";
foreach($details as $prop => $val)
	{
	$htm .= "
<span class=pmt-prop>".$prop."</span><span>".$val."</span><br />
";
	}
$htm .= "
<br />
</div>
";
$htm = "
<div class=pop-template>
	<div>&nbsp;</div><div>&nbsp;</div>
	<div class=pop-title>Payment Information</div>
	<div class=pop-side>
		<a href='javascript: adjust_payment();'>Adjust Payment</a>	
	</div>
	<div class=pop-content>
		".$htm."
	</div>
</div>
";
echo($htm);

?>