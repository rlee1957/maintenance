<?php

$path = "../";
include($path."../_t2/includes/session_start.php");  				// path good
include($path."configuration/environment_settings.php");			// path good
include($path."includes/should_errors_display.php");
include($path."../_t2/database/sql_functions.php");
ini_set('display_errors',1);  
error_reporting(E_ALL);
$customer_id = $_REQUEST["customer_id"];
$campaign_number = $_REQUEST["cid"];

if(isset($_REQUEST["order_id"]))
	{
	$order_id = $_REQUEST["order_id"];
	$sql = "
select * from order_list where order_id = ?
";
	$params = array($order_id);		
	}
else
	{
	$sql = "
select * from order_list where customer_id = ?
";
	$params = array($customer_id);	
	}

$results = sql_shell($sql, $params, $path);
/*echo($sql."<br />");
echo("<textarea style='width: 100%; height: 250px;'>");
print_r($results);
echo("</textarea>");
exit();*/
if($results["rowcount"] == 0)
	{
	$return["status"] = "No orders found for this customer!";	
	}
else
	{
	$orders = $results["recordset"][0];
	include($path."includes/get_campaign_details.php");
	include($path."includes/get_orders.php");		
	}

?>
