<?php

$in = "";
$del = "";
$order_id = $orders["order_id"];
$sql = "
select * from line_items_detail where order_id = ?
";
$params = array($order_id);		
$results = sql_shell($sql, $params, $path);
$order = $results["recordset"];
if($results["rowcount"] > 0)
	{
	if($campaign["campaign_type"] == "paver")
		{
		include($path."includes/show_paver_order.php");	
		}
	else
		{
		include($path."includes/show_fullfillment_order.php");
		}
	}

	
function get_line_item($items, $idx)
{
$return = "";
foreach($items as $ii => $item)
	{
	if($item["item_id"] == $idx)
		{
		$return = $item;
		break;		
		}
	}
return $return;	
}

function get_catalog_value($items, $idx, $field)
{
$return = "";
foreach($oi as $ii => $item)
	{
	if($item["item_id"] == $idx)
		{
		$return = $item[$field];
		break;		
		}
	}
return $return;	
}

function get_inscription($path, $inscription_id)
{
$sql = "
select inscription, status from inscriptions where inscription_id = ? order by line_number
";
$params = array($inscription_id);		
$results = sql_shell($sql, $params, $path);	
$return = array();
$return["status"] = $results["recordset"][0]["status"];
$text = "";
$del = "";
if($results["rowcount"] > 0)
	{
	foreach($results["recordset"] as $line => $inscription)	
		{
		$text .=	$del.$results["recordset"][$line]["inscription"];
		$del = "<br/>";
		}
	}
$return["inscription"] = no_break($text);
return $return;
}

function get_address_line($path, $shipping_id)
{
$sql = "
select * from shipping_data where shipping_id = ? order by line_number
";
$params = array($shipping_id);		
$results = sql_shell($sql, $params, $path);	
$return = "";
if($results["rowcount"] > 0)
	{
	$return .= "<span class=cap>".$results["recordset"][0]["full_name"]."</span> ";	
	$return .= strtoupper($results["recordset"][0]["address1"]). " ";
	$return .= strtoupper($results["recordset"][0]["city"]).", ";
	$return .= strtoupper($results["recordset"][0]["state_prov"])." ";
	$return .= strtoupper($results["recordset"][0]["postal_code"])." ";
	$return .= strtoupper($results["recordset"][0]["country"])." ";
	}
return $string = no_break($return);	
}

function no_break($str)
{
return 	str_replace(' ', '&nbsp;', $str);
}

?>