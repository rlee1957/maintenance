<table id="popup_ays" class="centered msg" style="display: none; font-size: 14pt;" cellspacing=25 cellpadding=20 border=0>
	<tr> <!-- space -->
		<td>
			<div class=popup-button-container style="display: block;">
				<img src="images/delete.png" onclick="hide_ays();" alt=exit class="popup-close no-select" />
			</div>
		</td>
	</tr>
	<tr> <!-- Popup Label -->
		<td align=center valign=middle>
			<span id=ays_label 
				  style="font-size: 20pt; font-weight: bold; color: #606060;">
				<?php echo($lang["page"]["cart_details"]["remove-popup"]["head"]); ?>
			</span>
		</td>
	</tr>
	<tr> <!-- space -->
		<td>&nbsp;<br />&nbsp;</td>
	</tr>
	<tr> <!-- Popup Instruction -->
		<td align=center valign=middle>
			<span id=ays_message>
				<?php echo($lang["page"]["cart_details"]["remove-popup"]["msg"]); ?>
			</span>
		</td>
	</tr>
	<tr> <!-- space -->
		<td>&nbsp;<br />&nbsp;</td>
	</tr>
	<tr> <!-- Popup Close -->
		<td align=right valign=bottom>
			<a id=ays_remove href="javascript: remove_item();" class=add-to-cart>
				<b><?php echo($lang["page"]["cart_details"]["remove-popup"]["remove"]); ?></b>
			</a>
		</td>
	</tr>
	<tr> <!-- space -->
		<td>
			&nbsp;
			<input type=hidden id=ays_next />
		</td>
	</tr>
</table>