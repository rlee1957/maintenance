<div id="popup_message" 
	 style="display: none;
			overflow: hidden; 
			padding: 0;
			position: fixed;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			max-width: 80%;
			max-height: 90%;
			background-color: #EBEBEB;
			padding: 20px;
			border-radius: 25px;">
	<div id="popup_button_container" 
		 style="position: relative;
				overflow: hidden;
				height: 35px;">
		<img id="popup_close" 
			 src="images/delete.png" 
			 onclick="hide_popup()" 
			 alt=exit 
			 class="popup-close no-select"
			 style="position: absolute;
					top: 5px;
					right: 5px;
					cursor: pointer;
					-webkit-touch-callout: none;
					-webkit-user-select: none;
					-khtml-user-select: none;
					-moz-user-select: none;
					-ms-user-select: none;
					-o-user-select: none;
					user-select: none;
					-webkit-tap-highlight-color: rgba(0,0,0,0);
					-webkit-touch-callout: none;"/>
	</div>
	<input type=hidden id="popup_next" />
	<div id=popup_label 
		 style="font-size: 18pt; 
				font-weight: normal; 
				color: #606060; 
				text-align: center;
				font-weight: bold;	
				padding: 0 25px 0 25px;"></div>
	<div style="height: 15px;"></div>
	<div id=popup_instruction 
		 style="font-size: 14pt;	
				color: #606060;
				text-align: center;
				padding: 0 25px 0 25px;">
	</div>
	<div style="height: 25px;"></div>
</div>