<input type=search 
	   <?php echo($element_events); ?> 
	   placeholder="<?php echo($element_placeholder); ?>" 
	   class="header-control"
	   style="	-webkit-appearance: none;
				outline: none;
				-webkit-user-select: none;
				width: 90%;
				-webkit-transition: width 0.8s ease-in-out;
				transition: width 0.8s ease-in-out;	
				border-style: none none solid none;
				border-width: 0 0 1px 0;
				border-color: #92959C;
				background-image: url('images/search.jpg');
				background-position: right center;
				background-repeat: no-repeat;
				background-size: 15px;"
	   id="<?php echo($element_id); ?>"
	   maxlength="<?php echo($element_maxlength); ?>"
	   title="<?php echo($element_title); ?>"
	   disabled />