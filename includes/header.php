<form id="frmHead" name="frmHead" method="post" target="work">
<table class="header-table" border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td class="header-field"><?php include("includes/select_a_campaign.php"); ?></td>
		<td class="header-field">
<?php
$element_id = "phone_search";
$element_placeholder = "Search by Phone Number";
$element_title = "Enter area code and phone number to submit search (only numeric characters, no spaces)";
$element_maxlength = "10";
$element_events = "
	   onkeyup='search_phone();'
";

include("includes/objects/search_box.php");
?>
		</td>
		<td class="header-field">
<?php
$element_id = "email_search";
$element_placeholder = "Search by Email Address";
$element_title = "Enter email address and press tab to submit search";
$element_maxlength = "255";
$element_events = "
	   onchange='search_email();'
";
include("includes/objects/search_box.php");
?>
		</td>
		<td class="header-field">
<?php
$element_id = "name_search";
$element_placeholder = "Search by Customer Name";
$element_title = "Enter name to search for";
$element_maxlength = "255";
$element_events = "
	   onkeyup='search_name();'
";
include("includes/objects/search_box.php");
?>
		</td>
		<td class="header-field">
<?php
include("includes/new_order_button.php");
?>		
		</td>
		<td class="header-field">
		
		</td>
	</tr>
	<tr style="height: 35px;">
		<td colspan=6>
<?php include("includes/campaign_identification.php"); ?>
		</td>
	</tr>
	<tr>
		<td class="header-field">
			ORDER
		</td>
		<td class="header-field">
			CUSTOMER
		</td>
		<td class="header-field">
			PAYMENT
		</td>
		<td class="header-field">
			TAXES
		</td>
		<td class="header-field">
			RESOURCES
		</td>
		<td class="header-field">
			OTHER
		</td>
	</tr>
</table>
</form>