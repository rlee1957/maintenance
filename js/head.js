var search_to;

function campaign_change()
{
check_save();
cs = document.getElementById("campaign_selector");
if(cs.value == ""){ return; }

frmHead = document.getElementById("frmHead");
ps = document.getElementById("phone_search");
es = document.getElementById("email_search");
ns = document.getElementById("name_search");
nob = document.getElementById("new_order_button");
hide_search_results();
ps.value = "";
es.value = "";
ns.value = "";
ps.disabled = false;
es.disabled = false;
ns.disabled = false;
nob.disabled = false;
ar = cs.value.split("|");
document.getElementById("cid").value = ar[0];
document.getElementById("campaign_id").value = ar[1];
document.getElementById("curl").value = ar[2];
document.getElementById("clabel").value = ar[3];
document.getElementById("ctype").value = ar[4];
cn = document.getElementById("campaign_name");
cn.innerHTML = ar[3];
}

function show_search_results(htm)
{
searchResults = document.getElementById("search_results");
searchResults.innerHTML = htm;
searchResults.style.display = "block";
}

function hide_search_results()
{
/*searchResults = document.getElementById("search_results");
searchResults.innerHTML = " ";
searchResults.style.display = "none";*/
}

function check_save()
{
	
}

function do_search(obj, force)
{
if(obj.id == "phone_search"){ search_phone(); }
if(obj.id == "email_search"){ search_email(); }
if(obj.id == "name_search"){ search_name();	}
}
/*
function get_customer(customer_id)
{
var form_data = "customer_id=" + customer_id;
var cid = document.getElementById("cid").value;
form_data += "cid="	+ cid;
var href = "actions/set_customer.php";	
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ check_search_results(result); }
		}
	);
}
*/

function search_phone()
{

var form_data = "";
var phone = document.getElementById("phone_search").value;	
if(phone.length >= 10)
	{
	var href = "actions/search_phone.php";
	var cid = document.getElementById("cid").value
	form_data += "cid=" + cid + "&phone=" + phone;	
	$.ajax
		(
			{
			url: href, 
			type: "POST",
			data: form_data,
			success: function(result){ check_search_results(result); }
			}
		);	
	}

}

function show_generic_results()
{
var object = JSON.parse(result);
var typ = object.type;
var htm = object.htm;
if(typ == "customer")
	{
		
	}
if(typ == "order")
	{
		
	}

if(typ == "payment")
	{
		
	}
	
}

function check_search_results(result)
{
var order = JSON.parse(result);
if(order.customer_rowcount == 1)
	{
	set_customer(order.customer_id);
	if(order.order_rowcount == 1)
		{
		get_order(order.order_recordset[0].order_id);
		return;
		}	
	if(order.order.rowcount > 1)	
		{
		show_order_list(order);	
		return;		
		}
	}
if(order.customer_rowcount > 1)
	{
	show_customer_list(order);	
	return;
	}
show_no_results(order);
return;
}

function show_all(order)
{
get_customer();	
//get_order();
//get_payment();
//get_shipping();
}

function show_decision()
{

hide_popup();	
document.getElementById("popup_background").style.display = "block";
document.getElementById("popup_decision").style.display = "block";
	
}

function show_ays()
{

hide_popup();	
document.getElementById("popup_background").style.display = "block";
document.getElementById("popup_ays").style.display = "block";
	
}

function show_message(title, message, next)
{

hide_popup();
document.getElementById("popup_label").innerHTML = title;
document.getElementById("popup_instruction").innerHTML = message;
document.getElementById("popup_next").value = next;	
document.getElementById("popup_background").style.display = "block";
document.getElementById("popup_message").style.display = "block";
	
}

function hide_popup()
{
	
document.getElementById("popup_background").style.display = "none";
document.getElementById("popup_background_click").style.display = "none";
document.getElementById("popup_message").style.display = "none";	
document.getElementById("popup_ays").style.display = "none";	
document.getElementById("popup_decision").style.display = "none";	
	
}

function show_order_list(order)
{
msg = "";
for(x=0;x<order.order.rowcount;x++)
	{
	msg += order.customer.recordset[x].full_name + " ";	
	msg += order.customer.recordset[x].address1 + " ";
	msg += order.customer.recordset[x].city + ", ";
	msg += order.customer.recordset[x].state_prov + " ";
	msg += order.customer.recordset[x].postal_code + " ";
	msg += order.customer.recordset[x].country + " ";
	msg += "\r\n";
	}
alert(msg);	
}

function show_customer_list(order)
{
msg = "<center><span class=popup-title>Select Customer</span><br />";
msg += "<span class=popup-instruction>Select the custome that is calling. Or click exit to perform another search.</span></center>";
msg += "<div style='width: 600px; overflow: auto; padding: 25px; border-style: solid; border: color: #000000; border-width: 1px; background-color: #FFFFFF;'><table cellpadding=25><tr>";
for(x=0;x<order.customer_rowcount;x++)
	{
	msg += "<td><div class=select-item onclick='set_customer(" + order.customer_recordset[x].customer_id + ");'>";
	msg += "<span style='text-transform: capitalize;'>" + order.customer_recordset[x].full_name + "</span><br />";	
	msg += order.customer_recordset[x].address1 + "<br />";
	msg += order.customer_recordset[x].city + ", ";
	msg += order.customer_recordset[x].state_prov + " ";
	msg += order.customer_recordset[x].postal_code + " ";
	msg += order.customer_recordset[x].country;
	msg += "</div</td>";
	}
msg += "</tr></table></div>";
msg += "<center><input type=button onclick='hide_popup();' value='EXIT' /></center>";
document.getElementById("popup_decision").innerHTML = msg;
show_decision();
}

function show_no_results(order, typ)
{
var msg = "Searching " + order.search_type + " (" + order.search_value + ")\rreturned no results!";
alert(msg);	
}

function search_email()
{
var form_data = "";
var email = document.getElementById("email_search").value;	
var href = "actions/search_email.php";
var cid = document.getElementById("cid").value
form_data += "cid=" + cid + "&email=" + email;	
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ check_search_results(result); }
		}
	);	
}

function search_name()
{
var name = document.getElementById("email_search").value;	
}

function search(obj_name, force)
{
cs = document.getElementById("campaign_selector");
obj = document.getElementById(obj_name);
if(cs.value == ""){ return; }
if((force)||(obj.value.length	> 3))
	{
	f = document.getElementById("frmHead");
	f.action = "do_search.php";
	f.submit();
	}	
}

function show_order(oid)
{
document.getElementById("oid").value = oid;
hide_search_results();	
f = document.getElementById("frmHead");
f.action = "get_order.php";
f.submit();
}

function toggle(nme)
{
if(document.getElementById(nme).style.display == "none")	
	{
	document.getElementById(nme).style.display = "block";	
	}
else
	{
	document.getElementById(nme).style.display = "none";	
	}
}

function show_popup()
{
document.getElementById('popgo').click();	
}

function create_new_order()
{
show_section("sec_new_order");
hide_search_results();	
f = document.getElementById("frmHead");
f.action = "create_new_order.php";
f.submit();
}

function show_section(dv)
{
document.getElementById("sec_order").style.display = "none";
document.getElementById("sec_new_order").style.display = "none";
document.getElementById("sec_resources").style.display = "none";
document.getElementById(db).style.display = "block";	
}  

function trim(str) 
{
return str.replace(/^\s+|\s+$/gm,'');
}

function toggle_slider(typ)
{

var obj = document.getElementById(typ);
var nv = "25px";
if(obj.style.left != nv)
	{ 
	hide_slider();
	show_slider(typ); 
	}
else
	{
	hide_slider();	
	}
	
}

function show_slider(typ)
{
var nv = "25px"; 
var item = "#" + typ;
$(item).animate({ left: nv }, 600); 
/*if(typ == "customer"){ $('#customer').animate({ left: nv }, 500); }
if(typ == "order"){ $('#order').animate({ left: nv }, 500); }
if(typ == "payment"){ $('#payment').animate({ left: nv }, 500); }*/
}

function hide_slider()
{
var nv = "-2000"; 
$('.window-slide').css('left', '-100%');
}

function set_customer(customer_id)
{

var form_data = "customer_id=" + customer_id;	
var href = "actions/set_customer.php";
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ fill_customer(result); }
		}
	);		
	
}

function set_order(customer_id)
{
var cid = document.getElementById("cid").value;
var form_data = "customer_id=" + customer_id + "&cid=" + cid;	
var href = "actions/set_order.php";
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ fill_order(result); }
		}
	);		
	
}

function set_payment(order_id)
{
var cid = document.getElementById("cid").value;
var form_data = "order_id=" + order_id + "&cid=" + cid;	
var href = "actions/set_payment.php";
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ fill_payment(result); }
		}
	);		
	
}

function fill_customer(result)
{

//hide_popup();	
document.getElementById("customer").innerHTML = result;
/*hide_slider("customer");
hide_slider("order");
hide_slider("payment");
show_slider("customer"); */
var customer_id = document.getElementById("customer_id").value;	
set_order(customer_id);

}

function fill_order(result)
{

hide_popup();	
document.getElementById("order").innerHTML = result;
/*hide_slider("customer");
hide_slider("order");
hide_slider("payment");
show_slider("order"); */
var order_id = document.getElementById("order_id").value;	
set_payment(order_id);
	
}

function fill_payment(result)
{

hide_popup();	
document.getElementById("payment").innerHTML = result;
hide_slider();
show_slider("customer");
	
}

function do_nothing()
{
// this function does nothing
}

