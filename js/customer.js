function country_change(add_typ, add_idx)
{
var obj_name = add_typ + "_country";
var obj = document.getElementById(obj_name);
var state, zip, phone;
if(obj.value != "")
	{
	if(obj.value == "US")
		{
		state = document.getElementById(add_typ + "_us_states").value;
		zip = document.getElementById(add_typ + "_us_zip").value;	
		}
	else
		{
		if(obj.value == "CA")
			{
			state = document.getElementById(add_typ + "_canada_provinces").value;
			zip = document.getElementById(add_typ + "_canada_postal_code").value;
			}
		else
			{
			state = document.getElementById(add_typ + "_other_divisions").value;
			zip = document.getElementById(add_typ + "_other_postal_code").value;			
			}	
		}
	document.getElementById(add_typ + "_state_container").innerHTML = state;
	document.getElementById(add_typ + "_postal_code_container").innerHTML = zip;
	}
	
	
}

function restore_look(obj)
{
obj.style.color = "#000000";
obj.style.fontWeight = "normal";
}