<?php
$creditcard = array();
$ar = array();
$ar["label"] = "MasterCard";
$ar["display"] = true;
$ar["disabled"] = "";
$creditcard["MasterCard"] = $ar;
$ar = array();
$ar["label"] = "Visa";
$ar["display"] = true;
$ar["disabled"] = "";
$creditcard["Visa"] = $ar;
$ar = array();
$ar["label"] = "Discover";
$ar["display"] = true;
$ar["disabled"] = "";
$creditcard["Discover"] = $ar;
$ar = array();
$ar["label"] = "AMEX";
$ar["display"] = true;
$ar["disabled"] = "";
$creditcard["AMEX"] = $ar;
/* # Disabled example
$ar = array();
$ar["label"] = "AMEX";
$ar["display"] = true;
$ar["disabled"] = "disabled";
$creditcard["AMEX"] = $ar;
*/
?>